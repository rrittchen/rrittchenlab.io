---
title: "Diagrams in Jekyll"
author: "Roland Rittchen"
categories:
  - Project
tags:
  - Blog
---

The setup of the blog is complete, but there is still times where small features are missing. This is one of these
cases. As one of my first posts revolves around the architecture of my trading bot I wanted to lay out the architecture
of the bot. I already experienced the comfort and clarity of showing the architecture in a series of diagrams called C4.
For Git readmes that is often done by a framework called Mermaid. With Mermaid you can write the diagrams as a code
block in the markdown file for the readme. Since Jekyll also uses markdown for its pages it is only natural to try and
use Mermaid here.

## C4 Diagrams

The [C4 model](https://c4model.com/) was created by Simon Brown between 2006 and 2011. It is based on the UML - unfied
modelling language. The name derives from covering 4 different levels of view on the architecture of your software:

- Context diagrams (level 1)
- Container diagrams (level 2)
- Component diagrams (level 3)
- Code diagrams (level 4)

Each of those 4 levels is named starting with a "C" hence C4.

I don't claim expertise on these diagrams. While the linked website and its examples make it seem very straight forward to use, in practise it is a bit harder. With smaller projects, or projects in the early stages (such as mine) it can be hard to appropriately describe the different levels. E.g. the context and container diagrams can be almost the same if you only have one or two containers. Similarly the container and component diagrams can have a strong overlap when you still have one component per container. For this reason I want to practise using these diagrams in this project.

## Mermaid

[Mermaid](https://mermaid.js.org/) is a JavaScript-based diagramming and charting tool that uses Markdown-inspired text definitions and a renderer to create and modify complex diagrams. It allows you to directly embed diagrams into your markdown readme files. The code is very easy and straight forward to learn and in no time, you will code your diagrams like a pro. Until then, Mermaid offers [an online live editor](https://mermaid-js.github.io/mermaid-live-editor/) that gives you instant feedback and debugging. Mermaid is already implemented in most plattforms and tools. From [VS Code](https://marketplace.visualstudio.com/items?itemName=bierner.markdown-mermaid) to [IntelliJ, and other Jetbrains products](https://plugins.jetbrains.com/plugin/20146-mermaid), from [GitHub](https://github.blog/2022-02-14-include-diagrams-markdown-files-mermaid/) to [GitLab](https://handbook.gitlab.com/handbook/tools-and-tips/mermaid/).

Be mindful though, the integrations differ in slight details. So always check the result in the framework of your choice instead of blindly trusting the source documentation.

## Setting up Mermaid in Jekyll

Jekyll is already markdown based, so it is well suited for Mermaid. The easiest and cleanest way to use it is with a plugin ([although it is possible without plugin](https://medium.com/@wangling1882/how-to-use-mermaid-in-jekyll-without-plugin-ec0c1255eb90)). I chose [Jekyll Spaceship](https://github.com/jeffreytse/jekyll-spaceship) as the more officially sounding [Jekyll Mermaid](https://github.com/jasonbellamy/jekyll-mermaid) isn't really maintained.

The bundler had issues when I tried running it locally. The reason in my case seemed to be a [conflict with the Bundler](https://github.com/jeffreytse/jekyll-spaceship/issues/52). So to fix that I deleted this line from Gemfile.lock:

```
BUNDLED WITH
   2.4.13
```

I reinstalled the bundler and the bundles:

```
gem install bundler
bundle install
```

And finally the easy sample diagram given by Mermaid worked!

```mermaid!
pie title Pets adopted by volunteers
  "Dogs" : 386
  "Cats" : 85
  "Rats" : 35
```

Next step was copying and trying a C4 diagram. (While Mermaid includes them, their syntax deviates from the easier diagrams, so a separate test is prudent)

```mermaid!
C4Context
      title System Context diagram for Internet Banking System
      Enterprise_Boundary(b0, "BankBoundary0") {
        Person(customerA, "Banking Customer A", "A customer of the bank, with personal bank accounts.")
        Person(customerB, "Banking Customer B")
        Person_Ext(customerC, "Banking Customer C", "desc")

        Person(customerD, "Banking Customer D", "A customer of the bank, <br/> with personal bank accounts.")

        System(SystemAA, "Internet Banking System", "Allows customers to view information about their bank accounts, and make payments.")

        Enterprise_Boundary(b1, "BankBoundary") {

          SystemDb_Ext(SystemE, "Mainframe Banking System", "Stores all of the core banking information about customers, accounts, transactions, etc.")

          System_Boundary(b2, "BankBoundary2") {
            System(SystemA, "Banking System A")
            System(SystemB, "Banking System B", "A system of the bank, with personal bank accounts. next line.")
          }

          System_Ext(SystemC, "E-mail system", "The internal Microsoft Exchange e-mail system.")
          SystemDb(SystemD, "Banking System D Database", "A system of the bank, with personal bank accounts.")

          Boundary(b3, "BankBoundary3", "boundary") {
            SystemQueue(SystemF, "Banking System F Queue", "A system of the bank.")
            SystemQueue_Ext(SystemG, "Banking System G Queue", "A system of the bank, with personal bank accounts.")
          }
        }
      }

      BiRel(customerA, SystemAA, "Uses")
      BiRel(SystemAA, SystemE, "Uses")
      Rel(SystemAA, SystemC, "Sends e-mails", "SMTP")
      Rel(SystemC, customerA, "Sends e-mails to")

      UpdateElementStyle(customerA, $fontColor="red", $bgColor="grey", $borderColor="red")
      UpdateRelStyle(customerA, SystemAA, $textColor="blue", $lineColor="blue", $offsetX="5")
      UpdateRelStyle(SystemAA, SystemE, $textColor="blue", $lineColor="blue", $offsetY="-10")
      UpdateRelStyle(SystemAA, SystemC, $textColor="blue", $lineColor="blue", $offsetY="-40", $offsetX="-50")
      UpdateRelStyle(SystemC, customerA, $textColor="red", $lineColor="red", $offsetX="-50", $offsetY="20")

      UpdateLayoutConfig($c4ShapeInRow="3", $c4BoundaryInRow="1")
```

## Alternatives

While I favour and like Mermaid, I also want to mention at least one alternative. [Draw.io](https://www.draw.io) offers a free diagram editor with extensive UML capabilities. There is also a [plugin](https://github.com/mutcher/jekyll-drawio) for Jekyll to use the native file format. Alternatively you can also just export diagrams in a .jpg or .png format and include them in your readme.

