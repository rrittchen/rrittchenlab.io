---
title: "Styling the Minimal Mistakes Theme"
author: "Roland Rittchen"
categories:
  - Project
tags:
  - Blog
---

For the final post in my series about the blog I want to talk about styling. Jekyll and the Minimal Mistakes theme made it easy to get a great looking blog online. Now the task is to make it truly my own. This means getting into the nitty gritty of the Sass for the theme and tweaking it till it represents me and my style.

## The Skin

The Minimal Mistakes theme comes out of the box with 10 great [skins](https://mmistakes.github.io/minimal-mistakes/docs/configuration/#skin). I have chosen the "sunrise" skin for it's soothing warm yellow tones. Though the "neon" skin was a strong second!

To select the skin you need to open the `_config.yaml` file in your main directory.

```Markdown
theme: "minimal-mistakes-jekyll"
minimal_mistakes_skin: "sunrise"  
```

This is what the configuration looks like for me.

## Title Logo

The header bar looks so bland without a logo, so let's add that.
{% raw %}
<figure>
<img src="https://rrittchen.gitlab.io/assets/images/posts/2023-06-12/title-image.jpg" alt="GitLab setup screenshot" class="full">
<figcaption>The title logo</figcaption>
</figure>
{% endraw %}
Fortunately this is just as easy as picking a skin! So once again open the `_config.yaml` file. And add something along the lines of:

```
logo: "/assets/images/favicon/android-chrome-192x192.png"
```

The logo itself I created with [favicon.io](https://favicon.io/favicon-generator/). The result will also come in handy in the next section.

## Page Icon

The favicons from the previous section now also get there proper place! When you open a website, or when you bookmark a page, you will realise there is a small image next to ti the name of the website.

The name is once again set in the `_config.yaml` like so:

```
title: "Code Blog"
```

To make the icons appear there is a little more work necessary though! First you create a new file under this path:

```bash
_includes\head\custom.html
```

Inside you set the icons:

```
<link rel="apple-touch-icon" sizes="180x180" href="/assets/images/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/assets/images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon/favicon-16x16.png">
<link rel="manifest" href="/assets/images/favicon/site.webmanifest">
<meta name="msapplication-TileColor" content="#f9b248">
<meta name="theme-color" content="#15141A">
```

Don't forget off course to place the icons in the referenced path in the assets folder.

## Images and Captions

The recommended way to handle images in blog posts goes like [this](https://mmistakes.github.io/minimal-mistakes/post%20formats/post-image-standard/). The images look great. However, coming from university reports and technical specs, I am used to have captions below an image.

As a first step I wrapped the simple `<img>` tag with a `<figure>` tag and then added a `<figcaption>` to it. The result looks like this:

```
{% raw %}
<figure>
<img src="https://rrittchen.gitlab.io/assets/images/posts/2023-06-08/Gitlab-setup.jpg" alt="GitLab setup screenshot" class="full">
<figcaption>The setup for this blog</figcaption>
</figure>
{% endraw %}
```

This already gives you images with a working caption. As captions weren't fully considered in the theme so the styling is not up to par with the rest of the theme. I disliked the large padding below the image and the serif font for the caption. So that's where I had to get my hands dirty with the styling.

Unfortunately there is no minimalist way to start with styling. With this theme you have to copy the entire "_sass" folder from the full [Minimal-Mistakes](https://github.com/mmistakes/minimal-mistakes) theme. Also the "css" folder with the `main.scss` file inside that handles the import of the Sass styling.

Once you managed that, you have now the full styling of the theme at your finger tips! For the figcaption the relevant files are now `_base.scss` starting line 246. Mine looks like this now:

```
figcaption {
  margin-bottom: 0.5em;
  color: $muted-text-color;
  font-family: $caption-font-family;
  font-size: $type-size-6;
  font-weight: bold;
  a {
    -webkit-transition: $global-transition;
    transition: $global-transition;

    &:hover {
      color: $link-color-hover;
    }
  }
  padding-bottom: 0.5em;
  border-bottom: 1px solid #baaa92;
  width: 100%;
}
```

The changes being the font-weight to bold, the padding-bottom and I also added a border underneath to separate it more from the following thext. For the font-family you will notice the "$" sign. This means it is referencing a variable which is set in the `_variables.scss`. I just changed the value for that variable from sarif to a sans-serif font.

## My Avatar Picture

In a similar way I changed the avatar image styling.

{% raw %}
<figure>
<img src="https://rrittchen.gitlab.io/assets/images/posts/2023-06-12/minimal-mistakes-avatar.jpg" alt="Minimal Mistakes Avatar" >
<figcaption>The original avatar styling wasn't to my liking</figcaption>
</figure>
{% endraw %}

{% raw %}
<figure>
<img src="https://rrittchen.gitlab.io/assets/images/posts/2023-06-12/my-avatar.jpg" alt="My Avatar" >
<figcaption>More square fits my style better</figcaption>
</figure>
{% endraw %}

I found all the options in the `_sidebar.scss` file starting line 136.

```
img {
    max-width: 200px;
    border-radius: 10%;

    @include breakpoint($large) {
      padding: 5px;
      //border: 1px solid $border-color;
    }
}
```

Little changes go a long way!

### Adding a link to the About Page

A nice extra is, when the avatar picture would link to the About page.
For this we need an `authors.yml` file in the _data folder. The content mainly duplicates what is already in the `_config.yml` about the author. But importantly, here you can set the "home" property with a link to the about page.
Now all you need is to add `author: "Roland Rittchen"` on top of every page where you set `author_profile: true`.

And thats it! Now you have seen it all.

## Conclusion

The previous sections have touched all of the following:

- Skin
- _config.yml
- Custom.html
- Sass

It was not a comprehensive guide by any means, but with these 4 different levers you have full control over almost the entire functionality and style of you Minimal Mistakes page.

The one last element we didn't get into is layouts. As of now I haven't found a need for it though. I will report back if I do.

Now go and make it your own!
