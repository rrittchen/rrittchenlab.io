---
title: "How to set up a Jekyll Blog on GitLab Pages"
author: "Roland Rittchen"
categories:
  - Project
tags:
  - Blog
---

There is already a wide galaxy of blog posts out there describing in detail how to set up your own blog for free. Most of it really is straight forward and we can all be grateful for all the high quality open source material out there. Still I want to present to you my own process, as I encountered a few traps which lead me to go through the process twice. I hope I can save someone from the headache of going through the same.

## Local Setup on a Mac

Personally I am working on this blog from both a Mac Book and a Windows PC. I will first present the setup guide for the Mac as it is significantly easier.
Assuming you already have Homebrew installed:

First you need Ruby:

```bash
brew install chruby ruby-install xz

ruby-install ruby 3.1.3
```

Then you install Jekyll:

```bash
gem install jekyll
```

And that's already it! Mac profits from many underlying Linux tools. Something that cannot be said for Windows. So currently when I work on Windows I too often just push to GitLab, and then look at the result on the actual page. (big no no, developing on the production environment)

## GitLab Pages

The majority of guides for a Jekyll blog are focusing on GitHub. I chose GitLab instead as I had already worked with GitHub and wanted to give the competition a chance. Besides, GitLab Actions is a valuable skill to get into.

Now when hosting on GitLab, you will want to use the shortest and most relevant URL. (that is unless you already have, or want to use a dedicated url) This is the first trap! As experienced GitHub user, I was quick to start a new project, put that project in a group to have everything nice and tidy, and the damage was done. GitLab is very specific on its system for urls.

GitLab further leads you into that trap by skipping this detail in the first tutorial for GitLab Pages. In the create a website from scratch it just says you need a blank project without giving further details. It is only in a separate [section](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html) where you learn about default domain names. (note the url for that page - it surely was meant as the first page you should read)

```html
{% raw %}
<figure>
  <img src="https://rrittchen.gitlab.io/assets/images/posts/2023-06-08/Gitlab-setup.jpg" alt="GitLab setup screenshot" class="full">
  <figcaption>The setup for this blog</figcaption>
</figure>
{% endraw %}
```

If you do it right you will find your blog under your-username.gitlab.io. This is also a big advantage for Jekyll and the theme for the blog, as you don't have to deal with relative links to resources.

## Minimal Mistakes Theme

Jekyll on its own is very barebones. But fortunately there is a large community providing themes and addons. One of the most popular and most complete themes is [Minimal Mistakes by Michael Rose](https://mmistakes.github.io/minimal-mistakes/). The documentation is pretty straight forward, but there is one lesson I learned. You don't need to go ahead and fork the full theme! Actually there is a second repo providing [just the necessary parts to start](https://github.com/mmistakes/mm-github-pages-starter). At a later point you can then add more and more bits of the full theme as you need them. I for one needed the Sass styling from the full theme for my customisation as I will describe in a follow up post.

Once you have copied the starter into your repo you need to do two things: You need a Gemfile, and you need to edit the _config.yml.

```
source "https://rubygems.org"

gem "minimal-mistakes-jekyll"
```

This is my Gemfile for reference.

For the _config.yml I don't want to give a full guide. You should really read it through line by line for yourself, as you will revert to it many times during the setup and extension of your blog!
The important part is though how the theme is setup. Here Minimal Mistakes provides multiple options which can be confusing when you start out. All my settings are for a local theme like this:

```
theme: "minimal-mistakes-jekyll"
minimal_mistakes_skin: "sunrise"
```

This should be enough to get you started locally. Next up the build pipeline to have it run on your GitLab Pages.

## GitLab CI

For the CI pipeline I tried many different things. Mostly out of frustration, because things were not working. The cause of the errors was eventually the relative path as I had originally set up a repo in a project called "blog".
I did find a wonderful example though by [Nithiya Streethran](https://nithiya.gitlab.io/) and her [repo here](https://gitlab.com/nithiya/nithiya.gitlab.io/-/blob/main/.gitlab-ci.yml). The original GitLab [documentation](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html) should work perfectly as well too.

## Other relevant topics

This post is not meant to be complete. While I don't want to get into everything in detail, there is a few more topics I should at least mention.

### Local Setup on Windows

Running everything on Windows suffers from the lack of the "make" and "GCC" tools. You can either try to install them on Windows, or you could make use of the [Windows Subsystem for Linux (WSL)](https://learn.microsoft.com/en-us/windows/wsl/install). I will not go into details but refer to a great blog post by [Anna Leonova](https://annjulyleon.github.io/guides/switch-to-mmistakes/). You will find a detailed description there. It is still a hassle as you are basically running two systems in parallel, but if you don't have access to a Mac or a full Linux machine this can work for you. In that case I would recommend developing in branches, pushing the branch on Windows, pulling it in your Linux subsystem and running it there, the localhost output you can again watch on Windows and change your code accordingly.

### Internationalisation

One more thing that Anna Leonova's blog shows is internationalization. The ability to have your blog multilanguage. Minimal Mistakes [is fully prepared](https://mmistakes.github.io/minimal-mistakes/docs/ui-text/) for that. I chose to stick with plain english though for now.

### Outlook on staticman

Jekyll provides you with a static page. This is all nice and fine if you just present something, but doesn't allow for interaction. Here [Staticman](https://staticman.net/) comes in. Staticman is basically a bot running somewhere else, that receives any comments posted under your blog posts, and then pushes the comments onto your GitLab (or GitHub) repo. Once the build pipeline has run through, the comment is then visible on your static page. Magic!

Staticman still recommends Heroku as hosting provider. Since Heroku has phased out their generous free tier I would recomment [fly.io](https://fly.io/) as alternative.

Once I implement staticman on my blog I will make a post about it!
