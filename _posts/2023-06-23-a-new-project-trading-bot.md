---
title: "A new project: Trading Bot"
author: "Roland Rittchen"
categories:
  - Project
tags:
  - V10
---

Every coder needs a side project. Side projects are a labour of love, a source of learning, and despite some frustrations also a great joy. I too have decided to start a larger side project and I want to lay out the reasons and goals in this post.

## Why doing a Trading Bot?

Trading Bots are by now quite a classic for programmers to try. And sure it is tempting to think, that you could get rich quick, if only this darn thing would work. I already did that once before in 2019-2020. I followed a [great series of blog posts](https://www.quantstart.com/articles/Event-Driven-Backtesting-with-Python-Part-I/) that showed an event driven trading bot in Python. My coding skills were insufficient then, and eventually I got over ambitious and the program execution slowed down to a crawl. I had in fact tried to calculate multiple indicators on each and every tick of data, and that with some very inefficient algorithms. Time for a fresh start. But first let me explain why.

### Personal Interest

My first reason is a passionate interest in the stock market for the last 20 years. I cannot claim to have made a lot of money. Most of the time I wasn't even invested in the market. But I always had a fascination for the stock market as a place where unlimited talent and unlimited resources were raging battles to see who can do it better. The smartest minds in the world devise algorithms to determine future prizes, billions are spent on infrastructure to have a split-second advantage in knowing prices.

I love working with numbers and I also love the deeply ingrained logic in how one news event affects dozens of different prices. I have by now given up the hope that any algorithm I could write would actually make money. But what I want is still to try coding it anyway and test it out in papertrading. At the very least I want to know just how far off I would have been. Another motivation is running back tests and assess probabilities. Once you have collected a large amount of data, the program can show different effects. E.g. what effect has El Niño on the prizes of pork futures in the following year. Sure the knowledge might be useless, but I enjoy learning.

### Technical Challenge

The other reason why trading bots are so popular for programers is that they pose great technical challenges.

- There is large amounts of data coming into a trading bot. Hundreds of different stocks, indices or derivatives. For each of these you can calculate dozens of indicators.
- The system is very time critical. The incoming data can be tick data, meaning there will be multiple updates on the price per second. Too much delay in the calculations and you lose money.
- Security is critical. While I never want to take my system live, I will still pretend, that this contains my actual money and that anybody with access could withdraw that money.
- Execution is critical. In many programs, losing a few datapoints is not critical. In games you will often have some lag, or items appearing and disappearing. Here forgetting about an open trade could cause large financial losses.

These challenges are exciting to me. They also make this project relevant as experience for the projects I do in my professional career. When I am working for clients in banking, real estate or healthcare the challanges are often similar.

## Requirements

While I have talked about trading bots very vaguely so far it is now time to specify my requirements. The bot has to:

- take in stock pricing and volumen on a tick level
- be later extensible to gather or accept other information (e.g. news, sentiment, stock analyses, investor reports)
- save the incoming data to be later used for backtests
- run forward performance testing - this means the backtesting data is fed in tick by tick, and the trading is simulated
- generate reports and charts to analyze the performance
- implement all steps and costs of live trading (e.g. transaction cost)
- run on a regular pc or home server (the data and computing loads might make cloud computing costly long term)

## Architecture

A few words on the architecture. In my dayjob I work a lot with microservices. I want to use this project to also gain experience useful in my day to day job. I also want this project to be a playground for the many many tools and services I discover in news reports, blogs, videos, and trade shows. This is the reason I will do it as a collection of microservices. Let me be clear: I know this is not the smartest or most efficient way to do it. If a client tasked me to do the same, I would very likely do it as a single monolithic application. But this is a private project and I am free to choose the hard way.

Thus this project will consist of many separate services with a single task each. I will use different programming languages and frameworks. I will tie it together with different ways of both synchronous and asynchronous communication. All of this will be containerized and run on a Kubernetes cluster. The backbone will be a central event store like Apache Kafka or Redpanda. (Actually this will be one of my learnings - comparing different event stores and their performance)

According to Sam Newman, who wrote the "bible" on microservices with: ["Building Microservices"](https://samnewman.io/books/building_microservices_2nd_edition/) the main advantages of microservices are the following:

- Technology Heterogeneity
- Robustness
- Scaling
- Ease of Deployment
- Composability

The disadvantages are:

- Developer Experience
- Technology Overload
- Cost
- Reporting
- Security
- Testing
- Latency
- Data Consistency

They don't work well for brand new products and startups. With unclear domain boundaries and requirements it is hard to slice your domain into separate parts. Mircroservices also carry a penalty when you have only a small team of developers.
So all of this clearly shows disadvantages in doing my side project as microservices. However I justify it with the chance to learn and practice this architectural pattern, to practice different techstacks, to practice orchestrating multiple components on a cluster and the modularity. I also think, that considering each and every single microservice as its own micro-side-project keeps my mental overhead smaller. For side projects you have context switching more often. When you work on the project on and off with extended breaks in between, it is probably easier to manage small entities, each of which you can grasp quickly.

## The Name

Finally the naming. Since there will be many different parts to the project that need naming, and since I have a background in automotive engineering I chose to name the project after an engine. The project name is V10. The separate microservices will be named after engine components. Each service will get a name relating to a suitable part with a similar function in an engine. E.g. the price data will be gathered by a microservice named "Intake". The backtesting data will be injected by a microservice name "Nitro" (for the nitrous oxide used to boost tuned cars). I hope this will help me to find useful names and still maintain a little overview over many different services.
