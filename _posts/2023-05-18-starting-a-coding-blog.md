---
title: "Starting a Code Blog"
author: "Roland Rittchen"
categories:
  - Project
tags:
  - Blog
---

So, I have started my personal coding blog. As you can see I chose [Jekyll](https://jekyllrb.com/) with the popular [Minimal-Mistakes theme](https://mmistakes.github.io/minimal-mistakes/) hostet on Gitlab Pages.
Let's now take a step back and see how and why I selected this option.

## The platform

First step as always was google. For example FreeCodeCamp has an article on [The Best Blogging Platforms for Developers](https://www.freecodecamp.org/news/best-blogging-platforms-for-developers/).
For me it boiled down to the following:

* **Wordpress:** The giant in the market. Both wordpress.org the free open source solution for self hosting and wordpress.com which hosts for you have been around for ages and power a large part of the internet. I have fiddled with a self hosted wordpress in the past and was dissatisfied with the themes and plugins. If you have something specific and fancy in your mind, it is an incredible hassle to make it look and feel right. Also the cost of self hosting or the hosted solution is around 5$ a month.
* **Medium:** The fashionable solution. Medium is an instant turn off for me. Often when searching for a coding problem you land on a medium blog, and get blocked by the paywall. Writing can be free, however readers mostly need to pay. I find this absurd. Also it leaves you no access to the design of the page, no custom domain, and I have read, that moving your content elsewhere is cumbersome to impossible.
* **Dev.to:** Like medium a newer kid on the block. More focused target group and lively community. Same as Medium though with a strong vendor lock in.
* **Hashnode:** Similar to the above mentioned a modern solution focused on a community. The advantage being that it is free and it gives you the option to export your posts to GitHub, so the lock in is not as bad. You can also use a custom domain. Eventually you are still dependent on the plattform when it comes to features or styling options or the terms of service.
* **Jekyll:** Jekyll is attractive as it is free and very customisable. As a static page it can be hosted for free on GitHub pages or GitLab pages. Since I am writing about my code it is only natural to keep the blog close to the code. I also don't expect a large readership and don't see a need to attract one either.

Eventually the plan is to use the RSS Feed that Jekyll provides and connect that to Dev.to and maybe also Hashnode. This gives me the best of both worlds as I still have full controll over the content and can never be locked out from the source. It also gives me access to larger communities where I can share and attract larger reader groups.
