---
title: "V10: The First Iteration"
author: "Roland Rittchen"
categories:
  - Project
tags:
  - V10
  - Architecture
---

The ambition of my V10 trading bot project is grand. Even
with an optimistic estimate, this will take years to fully
implement. So I want to cut the whole into smaller phases.
Here I will describe phase 1, the first iteration.

## Phase 1

In the first phase I will focus on the trading data. For any
further functionality, the bot needs pricing data. This is
the central element. The first Microservice therefore is the
**"Intake"**. (look at
my [last post](https://rrittchen.gitlab.io/project/a-new-project-trading-bot/#the-name)
to understand the naming scheme) This service will subscribe
to different stock pricing services or brokers (the first
one of which will be [Oanda](https://www.oanda.com/eu-en)),
format the data according to the internal data model and
publish it on the trading bots message stream.

The second microservice will use historical pricing data,
saved in a csv format to act as a mock stock pricing
service. This will help the development of the other
services, by providing large amounts of data at any time
and in a repeatable fashion. Comparative testing of
different algorithms becomes possible. I call this service *
*"Nitro"** for the N<sub>2</sub>O injection tuned cars use
to temporarily increase power. You will most likely have
seen that in the Fast and Furious movies.

As a third and final element for the first phase is a
database service. This will take all the pricing
information that is put on the message stream and save
it in a database. Through and endpoint the data can be
downloaded and stored and eventually be used as historical
data for the Nitro service. Note: while I will use a
database here, I plan to make extensive use of the
downloading capability. During the development it makes no
sense to have the hole project deployed permanently. So the
whole system including the database will be repeatedly
deployed and taken down again. This is also useful as it
provides a first consumer to the message stream and with the
download in the end a good check if the input csv and the
output csv match. I will call this service **"EGR"** for
exhaust gas recirculation. This is a technique where a part
of an engines exhaust gas is injected in the fresh air in
the intake. This significantly improves the emission, as
e.g. remaining carbohydrates in the exhaust gas will burn up
during their second cycle through the engine.

So the first phase has **a data source, a publisher, and
a consumer**. The circle is closed.

Following, I will try to show the structure of this
first phase in a series of diagrams
called [C4](https://c4model.com/). Showing the systems in (
up to) 4
different levels: System Context, Container Diagram,
Component Diagram, and Code Diagram. I will write more on
the C4 Model in a separate blog post.

### Context

The system context diagram in this phase is rather empty.
There is one external system, that is the online broker
which is providing pricing data. (Note, I have displayed one
broker, the system will be built to work with multiple
brokers concurrently though.) At this point no user
interaction with the system is implemented. I am building
the infrastructural backbone. In future phases this diagramm
will be extended with multiple user interfaces and other
external systems that provide data.

```mermaid!
C4Context
        title System Context diagram for the V10 Trading Bot

        System(SystemAA, "Online Brokerage")


        System(v10, "V10")

        Rel(SystemAA, v10, "Provides pricing data", "HTTP SSE")

        UpdateLayoutConfig($c4ShapeInRow="1")
```

### Container

The container diagram shows better how the microservices
within the bot work together. Intake will take the data from
the external broker and publish it on the message bus. Nitro
can act as a mock provider. EGR will consume data from the
bus and save it onto its database.

```mermaid!
C4Container
        title System Context diagram for the V10 Trading Bot

        System(SystemAA, "Online Brokerage", "Provides pricing data")

        System_Boundary(b, "V10 Trading Bot") {
            Container(intake, "Intake")
            Container(nitro, "Nitro")
            ContainerDb(nitrodb, "Nitro Cache")
            ContainerQueue(bus, "Message Bus")
            Container(egr, "EGR")
            ContainerDb(egrdb, "EGR Database")
        }

        Rel(SystemAA, intake, "Provides pricing data", "HTTP SSE")
        Rel(nitro, intake, "Provides pricing data", "HTTP SSE")
        BiRel(nitro, nitrodb, "Stores data for quick re-runs")
        Rel(intake, bus, "Publishes data")
        Rel(egr, bus, "Consumes data")
        Rel(egr, egrdb, "Stores data")
        

        UpdateLayoutConfig($c4ShapeInRow="3")
        UpdateRelStyle(SystemAA, intake, $offsetY="-50", $offsetX="10")
        UpdateRelStyle(nitro, intake, $offsetY="-50", $offsetX="-50")
        UpdateRelStyle(intake, bus, $offsetY="-10", $offsetX="-140")
        UpdateRelStyle(nitro, nitrodb, $offsetY="-10", $offsetX="60")
        UpdateRelStyle(egr, egrdb, $offsetY="-20", $offsetX="-30")
```

### Component and Code

There will be separate blog posts for each of the services.
I will document the component and code layout in these
posts. I don't think it makes much sense to plan these out
prematurely.

## Phase 2

While only speculation at this point the most logical plan
for phase two would be the following:

The raw stock pricing data, provided on a tick level (so
several price updates per second), need to be converted to
the different timeframes. E.g. 1 minute, 5 minute, 1 hour,
etc. . This will then form the basis for the famous candle
charts.
So there would be services or serverless functions
converting the pricing data. I also want to provide a front
end to display the pricing data in charts.

## Phase 3 and further out

Hopefully by phase three I shall calculate different
indicators like moving averages. These will also show up in
the front end. Based on the indicators I can then also write
a first version of a rule engine that decides when to buy
and when to sell. At this point there would only be
recommendations, no actions. The logic for buying will not
yet be implemented and the transaction cost wont be taken
into account.

Further out I need to build the buying and selling actions,
the logic for the cash account, and the transaction cost.
