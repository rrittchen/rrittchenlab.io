---
permalink: /about/
title: "About"
excerpt: "About the Author."
toc: true
---

## About Me

A former mechanical engineer, I transitioned towards software engineering during the pandemic. Now with a year of professional experience in the new field it is time to expand my learning efforts. I have a wide range of interests, and look forward to combining them into exciting projects. Also I would like to use these projects to get familiar with the devops and cloud engineering fields, as this is where I would love to move to longterm.

If you are looking for the dry and structured facts please head over to [LinkedIn](https://www.linkedin.com/in/roland-rittchen/).


## About the Blog

As my interests are eclectic, I want to use this blog to structure my thoughts. I also want to document my projects properly and practice better writing in both documentation and external communication. In this regard this blog is rather self serving. If anyone stumbles here, I still hope you will find value in my posts.