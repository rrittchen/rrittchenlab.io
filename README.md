# Blog

My personal coding blog. Find more details about the why and how online on the [blog](https://rrittchen.gitlab.io/)


## Acknowledgements

 - [jekyll is the technical base for this easy to use static page blog](https://jekyllrb.com/)
 - [Minimal Mistakes by Michael Rose makes the blog look good](https://mmistakes.github.io/minimal-mistakes/)
 - [GitLab is hosting it for free](https://gitlab.com/)


 ## Installation

Install prerequisites

- Ruby version 2.5.0 or higher
- RubyGems
- GCC and Make

Install the jekyll and bundler gems

```bash
  gem install jekyll bundler
```

Clone from GitLab

```bash
  git clone https://gitlab.com/rrittchen/rrittchen.gitlab.io.git
```


## Run Locally

Build the site and make it available on a local server

```bash
  bundle exec jekyll serve
```

Browse to http://localhost:4000


<h2 align="left">Languages and Tools</h2>
<br/>
<p align="left"> 
    <a href="https://sass-lang.com" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/sass/sass-original.svg" alt="sass" width="40" height="40"/> </a> 
    <a href="https://www.w3.org/html/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40"/> </a> 
    <a href="https://www.ruby-lang.org/en/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/ruby/ruby-original.svg" alt="ruby" width="40" height="40"/> </a> 
</p>

<!--- 
For structure - https://readme.so/editor
For languages and tools - https://rahuldkjain.github.io/gh-profile-readme-generator/
--->